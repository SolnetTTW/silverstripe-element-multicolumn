(function($) {
  $.entwine('ss', function($){
      $('#Form_ItemEditForm_ColumnType_Holder').entwine({
          onadd:function(){
              this.toggleFields();
          },
          onchange:function(){
              this.toggleFields();
          },
          toggleFields:function(){
              if(this.find('input:checked').length < 1){
                  this.find('input').first().attr('checked',true);
              }

              var value = this.find('input:checked').val(),
              grids = this.closest('.tab').find('.grid-field'),
                  biosCheckbox = $('#Form_ItemEditForm_ShowBios_Holder');

              switch(value){
                  case 'Solnet\\Elements\\MultiColumn\\CoreLink':
                      grids.hide();
                      biosCheckbox.hide();
                      $('#Form_ItemEditForm_CoreLinks').show();
                      break;
                  case 'Solnet\\Elements\\MultiColumn\\LargePageLink':
                      grids.hide();
                      biosCheckbox.hide();
                      $('#Form_ItemEditForm_LargePageLinks').show();
                      break;
                  case 'Solnet\\Elements\\MultiColumn\\People':
                      grids.hide();
                      biosCheckbox.show();
                      $('#Form_ItemEditForm_People').show();
                      break;
                  case 'SilverStripe\\Blog\\Model\\BlogPost':
                      grids.hide();
                      biosCheckbox.hide();
                      $('#Form_ItemEditForm_BlogPosts').show();
                      break;
              }
          }
      });
  });
}(jQuery));
