# Solnet Elemental Multi-Column Element

## Solnet team details

Tech lead: Darren Inwood (darren.inwood@solnet.co.nz, 021 555 6543)

## Overview of the project

Provides a multi-column element to display inside the SilverStripe CMS via the
DNA Elemental module.

## Requirements

* SilverStripe 4.x
* DNA Elemental 2.x

## Installation

To install:

```composer require 'solnet/silverstripe-element-multicolumn'```

## Configuration

Each Core Link and Large Page Link can have a colour picker added.

To do so, add some config YAML to your project:

```
# Available colour options for "Multi Column" element types
#   classname: Label in CMS
Solnet\Elements\MultiColumn\CoreLink:
  color_options:
    orange: Orange
    black: Black
    white: White
Solnet\Elements\MultiColumn\LargePageLink:
  color_options:
    orange: Orange
    black: Black
    white: White
```

## Git branching strategy

This project uses the Solnet Git Branching Strategy, which is Gitflow with Pull Requests
for all merges to the ''develop'' branch.

All development should be done in a feature branch named after the JIRA issue; JIRA issues
should have 'Create branch' buttons that will create the branch for you, but if not, the branch
should be named eg. ''feature/ABCD-1234''.

All commit messages should start with the JIRA ticket number eg. ''ABCD-1234 Added widget editing''.

Commit and push your feature branch, and use the Bitbucket web interface to create a Pull Request
to merge into the ''develop'' branch.

## Deployment

Releases and hotfixes are created using gitflow branches merged into master branch.

To create a new "release" version containing new features, follow this process:

1. Create release branch off ''develop'' named ''release/X.Y.Z'' - use Semver to determine the next version number, typically Y would be incremented and Z set to 0
2. Merge release branch into master
3. Create version number tag ''X.Y.Z'' matching the release branch
4. Merge changes from release branch into develop branch (to pull in hotfixes)
5. Delete release branch (optional)

To create a new "hotfix" version containing bugfixes, follow this process:

1. Create hotfix branch off ''master'' named ''hotfix/X.Y.Z'' - use Semver to determine the next version number, typically Z would be incremented
2. Merge hotfix branch into master
3. Create version number tag ''X.Y.Z'' matching the hotfix branch
4. Merge changes from hotfix branch into develop branch
5. Delete hotfix branch (optional)
