<?php

namespace Solnet\Elements;

use DNADesign\Elemental\Models\BaseElement;
use Silvershop\HasOneField\HasOneButtonField;
use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\ORM\ArrayList;
use SilverStripe\ORM\ManyManyList;
use SilverStripe\View\Requirements;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;
use Solnet\Elements\MultiColumn\CoreLink;
use Solnet\Elements\MultiColumn\LargePageLink;

/**
 * Element for the DNA Elemental module that displays a set of cards.
 */

class ElementMultiColumn extends CustomBaseElement
{
    use Configurable;

    private static $table_name = 'ElementMultiColumn';
    private static $singular_name = 'Multi Column Content';
    private static $plural_name = 'Multi Column Content';
    private static $description = 'Multi Column Content Module';

    private static $icon = 'font-icon-block-layout';

    private static $db = [
        'Color' => 'Varchar',
        'ColumnType' => 'Varchar',
    ];

    private static $many_many = [
        'LargePageLinks' => LargePageLink::class,
        'CoreLinks' => CoreLink::class,
    ];

    private static $many_many_extraFields = array(
        'CoreLinks' => array('SortOrder' => 'Int'),
        'LargePageLinks' => array('SortOrder' => 'Int')
    );

    private static $cascade_duplicates = [
        'LargePageLinks',
        'CoreLinks',
    ];

    /**
     * The text option displayed in the DNA Elemental type 'Add element' dropdown.
     *
     * @return string
     */
    public function getType()
    {
        return _t('Elements.SolnetMultiColumn_BlockType', 'Multi Column');
    }

    /**
     * Creates the editing interface for this element.
     *
     * @return SilverStripe\Forms\FieldList
     */
    public function getCMSFields()
    {
        Requirements::javascript('solnet/silverstripe-element-multicolumn:client/dist/js/ElementMultiColumnCMS.js');

        $fields = parent::getCMSFields();

        $fields->removeByName(array('LargePageLinks', 'CoreLinks', 'ColumnType'));

        $fields->addFieldsToTab(
            'Root.Main',
            [
                DropdownField::create(
                    'MaxColumns',
                    _t('Elements.SolnetMultiColumn_MaxColumns_Title', 'Maximum Number Of Columns'),
                    $this->config()->max_column_options // set in YAML
                )
                ->setDescription(
                    _t(
                        'Elements.SolnetMultiColumn_MaxColumns_Description',
                        'Controls how many items are displayed.  This allows you to create '.
                        'many options, and control which are displayed by re-ordering them.'
                    )
                ),
            ]
        );

        // If we have configured some colours, show colour picker
        $colorOptions = $this->config()->color_options;// set in YAML
        if ($colorOptions && is_array($colorOptions)) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    OptionsetField::create(
                        'Color',
                        _t('Elements.SolnetMultiColumn_Color_Title', 'Background color'),
                        $colorOptions
                    )->setEmptyString(
                        _t('Elements.SolnetMultiColumn_Color_EmptyString', '(Default)')
                    ),
                ]
            );
        }

        $types = $this->config()->data_types; // set in YAML
        if ($this->exists() && is_array($types)) {
            $fields->addFieldsToTab(
                'Root.Main',
                [
                    OptionsetField::create(
                        'ColumnType',
                        _t('Elements.SolnetMultiColumn_ColumnType_Title', 'Column Type'),
                        $types
                    ),
                    GridField::create(
                        'CoreLinks',
                        _t('Elements.SolnetMultiColumn_CoreLinks_Title', 'Core Links'),
                        $this->CoreLinks(),
                        $coreLinksConfig = GridFieldConfig_RelationEditor::create()->addComponent(
                            new GridFieldOrderableRows('SortOrder')
                        )
                    ),
                    GridField::create(
                        'LargePageLinks',
                        _t('Elements.SolnetMultiColumn_LargePageLinks_Title', 'Large Page Links'),
                        $this->LargePageLinks(),
                        $largePageLinksConfig = GridFieldConfig_RelationEditor::create()
                        ->addComponent(new GridFieldOrderableRows('SortOrder'))
                    ),
                ]
            );
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('Elements.SolnetMultiColumn_SavingTip', '<p class="message warning">Please save to see more options.</p>')
                )
            );
        }

        return $fields;
    }

    /**
     * Get the child records that match the 'Column type' selector, ie Large Page Links,
     * Core Links.
     *
     * @return SilverStripe\ORM\SS_List
     */
    public function getColumnRecords()
    {
        $type = array_search($this->ColumnType, $this->config()->many_many);
        if ($type) {
            return $this->$type()->sort('SortOrder');
        }
        return ArrayList::create([]);
    }

    /**
     * Intended for use in templates.  Outputs the first n children,
     * up to the limit set by the MaxColumns selector.
     *
     * @return SilverStripe\ORM\DataList
     */
    public function getLimitedRecords()
    {
        return $this->getColumnRecords()->limit($this->MaxColumns);
    }

    /**
     * Ensure that after creating a duplicate of this element, the duplicate
     * contains copies of all many_many relations.
     *
     * Slides attached to the duplicate are linked to new copies
     * of the original's slides, so that editing slides on the new copy doesn't
     * affect slides on the original.
     *
     * @param ElementMultiColumn $original The object just cloned
     * @param boolean $doWrite whether or not to write the clone to the database
     * @param array|null $relations
     */
    public function onAfterDuplicate($original, $doWrite, $relations)
    {
        if (!is_array($relations)) {
            $relations = [];
        }
        foreach ($relations as $relationFieldName) {
            // Get list of items from original
            $source = $original->getManyManyComponents($relationFieldName);
            $dest = $this->getManyManyComponents($relationFieldName);
            // Remove the already-duplicated items; we're about to duplicate them again
            $dest->removeAll();
            // Get extrafield names (eg. copy the sort order)
            if ($source instanceof ManyManyList) {
                $extraFieldNames = $source->getExtraFields();
            } else {
                $extraFieldNames = [];
            }
            // Loop through items from original and add to copy
            // with matching extrafields
            foreach ($source as $sourceItem) {
                $clone = $sourceItem->duplicate($doWrite);
                $extraFields = [];
                foreach ($extraFieldNames as $fieldName => $fieldType) {
                    $extraFields[$fieldName] = $sourceItem->getField($fieldName);
                }
                $dest->add($clone, $extraFields);
            }
        }
    }
}

