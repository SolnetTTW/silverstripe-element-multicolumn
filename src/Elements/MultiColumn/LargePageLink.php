<?php

namespace Solnet\Elements\MultiColumn;

use SilverShop\HasOneField\HasOneButtonField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\OptionsetField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;

/**
 * Represents a 'Card' displayed inside a Multi-Column element
 */

class LargePageLink extends DataObject
{
    use Configurable;

    private static $table_name = 'LargePageLink';

    private static $db = [
        'Title' => 'Varchar(255)',
        'Content' => 'Text',
        'Color' => 'Varchar(255)',
    ];

    private static $has_one = [
        'Image' => 'SilverStripe\Assets\Image',
        'CTALink' => 'Solnet\AdvancedLink\AdvancedLink',
    ];

    private static $summary_fields = [
        'Title' => 'Title',
    ];

    /**
     * @config
     * The available options for your theme as an associative array.
     * 'classname' => 'Label'
     */
    private static $color_options = [];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();

        $fields->removeByName('CTALinkID');

        $fields->addFieldsToTab(
            'Root.Main',
            [
                TextField::create(
                    'Title',
                    _t('Elements.SolnetMultiColumn_LargePageLink_Title_Title', 'Title')
                ),
                $content = TextareaField::create(
                    'Content',
                    _t('Elements.SolnetMultiColumn_LargePageLink_Content_Title', 'Content')
                ),
                OptionsetField::create(
                    'Color',
                    _t('Elements.SolnetMultiColumn_LargePageLink_Color_Title', 'Background color'),
                    $this->config()->color_options
                )->setEmptyString(
                    _t('Elements.SolnetMultiColumn_LargePageLink_Color_EmptyString', '(Default)')
                ),
            ]
        );
        $content->setRows(5);

        // If we haven't configured any colours, don't show colour picker
        if (!$this->config()->color_options) {
            $fields->removeByName('Color');
        }

        $fields->dataFieldByName('Image')->getValidator()->setAllowedExtensions(array('jpg', 'svg', 'png', 'jpeg'));

        if ($this->exists()) {
            $fields->addFieldToTab(
                'Root.Main',
                HasOneButtonField::create(
                    'CTALink',
                    _t('Elements.SolnetMultiColumn_LargePageLink_CTALink_Title', 'Link'),
                    $this
                )
            );
        } else {
            $fields->addFieldToTab(
                'Root.Main',
                LiteralField::create(
                    'SavingTip',
                    _t('Elements.SolnetMultiColumn_LargePageLink_SavingTip', '<p class="message warning">Please save to see more options.</p>')
                )
            );
        }

        return $fields;
    }
}
